PYGAME SIMPLIFIER
(c) Mikhail Shubin
2pi360@gmail.com


Introduction
============

Using `pygame` can be cumbersome sometimes.  `pygame_simplifier` module simplifies the use of `pygame` by providing shortcuts for the most used function and turning game coding into `Event-driven programming`.  This simplification is not comprehensive - I developed it for my own games first -- But it is designed to be easily modified and adopted.

Simplest possible code:

    import simplified_pygame

    WINDOW = simplified_pygame.PyGameWindow(640, 480)

    for events, time_passed, key_pressed in WINDOW.main_loop(framerate=60):
         pass

This code creates a windows, which can be closed.

Simple code with interactivity would look like this:


    import simplified_pygame

    WINDOW = simplified_pygame.PyGameWindow(640, 480)

    class Box(simplified_pygame.EventReader):

        def __init__(self):
            self.x = 0
            self.y = 0

        def on_key_right(self):
            self.x+=1
        def on_key_left(self):
            self.x-=1
        def on_key_up(self):
            self.y-=1
        def on_key_down(self):
            self.y+=1

    box = Box()

    for events, time_passed, key_pressed in WINDOW.main_loop(framerate=60):
         box.read_events(events, time_passed, key_pressed)
         WINDOW.rect((self.x*10+100, self.y*10+100, 10, 10), col=(255, 0, 0))

This would start window with small rectangle, reacting on arrow keys

System function can be implemented like this:

    import simplified_pygame

    WINDOW = simplified_pygame.PyGameWindow(640, 480)

    class AppControlls(simplified_pygame.EventReaderAsClass):

        def on_key_f1():
            WINDOW.set_screen_resolution(1)
        def on_key_f2():
            WINDOW.set_screen_resolution(2)
        def on_key_f3():
            WINDOW.set_screen_resolution('fullscreen')
        def on_key_escape():
            WINDOW.exit()

    for events, time_passed, key_pressed in WINDOW.main_loop(framerate=60):
         AppControlls.read_events(events, time_passed, key_pressed)

This would create a window which closes on pressing `escape` and changes a resolution on pressing `f1`, `f2` and `f3`.



Installation
============

Copy this file `pygame_simplifier.py` into your local folder.



Initialising pygame and the Main Loop
=====================================

To run a game, create a single instance of `simplified_pygame.PyGameWindow`.

    simplified_pygame.PyGameWindow(self, w, h, *, caption='my game', use_icon=False, on_exit=exit_pygame, bg_color=(0, 0, 0), default_font=None, resizable=False)

arguments: `w` and `h`: width and height of the window; `caption`: caption of the window; `use_icon`: whatever to load `icon.ico` from the assets local folder; `on_exit`: function to run when exit button on the window is pressed; `bg_color`: background color; `default_font`: default font to be used; `resizable`: whatever window can be resized.

to run the main loop, call

    for events, time_passed, key_pressed in WINDOW.main_loop(framerate):

here `framerate` is the desired number of loops per second. `main_loop` returns:

`events`: list of user input happened during the last loop. Events dont use `pygame` codes, but simple strings.

`time_passed`: integer, number of milliseconds passed since last loop.

`key_pressed`: set of keys (represented by strings), pressed at the end of the last loop.



Event-driven programming
========================

To used the benefits of event-driven programming paradigm, (1) inhering your custom class from `simplified_pygame.EventReader` (2) create an instance of this class and (3) call `read_events(events, time_passed, key_pressed)` on this instance from the main loop. Object's methods with the certain specific names would be called in response to user actions.


Keyboard and controller keys
----------------------------

### `.on_key_<key-name>(self)`
would be called every time the corresponding key is pressed.


### `.on_hold_<key-name>(self, total_duration, time_passed)`
would be called every loop when the corresponding key is hold down. This event handler receive two positional integer arguments: first is the total number of milliseconds when the key was pressed, second is duration of the last loop. The first argument can also be accessed from instance's `self._hold_duration[key]`. Here is a list of implemented keys:

* arrow keys:  `left`, `right`, `up`, `down`;
* keyboard letters numbers:  `a`, `b` ... `z`;
* numbers:  `0`, `1` ... `9`;
* f-keys:  `f1`, `f2` ... `f12`;
* `+` and `-`: `plus`, `minus`;
* other keys: `enter`, `escape`, `space`, `shift`, `ctrl`, `alt`;
* controller keys: `joy_A`, `joy_B`, `joy_X`, `joy_Y`, `joy_LB`, `joy_RB`, `joy_back`, `joy_start`;
* controller's dpad: `dpad_left`, `dpad_right`, `dpad_up`, `dpad_down`.

I only implement the support of a single controller, as I have a single controller.

Keys can be remaped by modifying `.key_map` dictionary. For example, with `.key_map == {'w': 'up'}` pressing both `w` and `up` will trigger `.on_key_up()` event handler, and holding
both `w` and `up` will trigger `.on_hold_up()` (once per loop, even if both keys are pressed). If you would like to disable a button, assign it to None, eg
when `.key_map == {'w': 'up', 'up': None}` only pressing `w` would trigger `.on_key_up()`.


### `.on_any_key(self, key)`

would be called every time the any key is pressed. Note that if handler for this particular key is also implemented, it would also be called after `on_any_key`.


Mouse
-----

### `.on_mouse_move(self, pos)`
### `.on_mouse_click(self, pos)`
### ` on_mouse_midclick(self, pos)`
### `.on_mouse_rightclick(self, pos)`
### `.on_mouse_wheel_up(self, pos)`
### `.on_mouse_wheel_down(self, pos)`
would be called every time the corresponding events happens. Methods are give a single positional argument: `pos`, which is a tuple `(x, y)` of the mouse coordinates. Coordinates are given relative to the in-game resolution, so one should not care about the screen resolution. If `.mouse_map` is defined, `pos` is replaced by `.mouse_map(x, y)`.

Mouse keys can also be remaped or disabled by modifying `.key_map` dictionary, eg with `.key_map == {'mouse_rightclick': 'mouse_click'}`. Remapping between keyboard and mouse
keys is possible, but remember that mouse event handles are given one positional argument while keyboard events are not.

### `.mouse_map(self, x, y)`
when this method is defined, the positional argument given to all mouse-related handlers is changed to `.mouse_map(x, y)` instead of `(x, y)`. If `mouse_map` returns None, handles except `.on_mouse_move` are not called at all.

This could be useful when, for example, implementing buttons:

    def mouse_map(self, x, y):
        if 100 < x 200 and 100 < y < 200: return 'button1'
        if 100 < x 200 and 300 < y < 400: return 'button2'
        return None

    def on_mouse_click(self, button):
        # here button would be either 'button1' or 'button2',
        # otherwise method is not called at all
        # ...

    def on_mouse_move(self, button):
       # here button would be 'button1', 'button2' or None
       # ...

Implementing `.mouse_map`, would also initialize a `._mouse_pos` attribute which would keep the latest mapped position of the mouse.


Timer
-----

### `.update(self, time_passed)`
If implemented, this method would be triggered every loop. Receive one positional integer argument: number of milliseconds passed since last loop.


### `.on_repeat_every_<milliseconds>(self)`
would be called every `<milliseconds>`, here `<milliseconds>` should be integer, for example method can be called `.on_repeat_every_1000(self)` to be called every second. This handler is called only once per loop, even if a multiple of `<milliseconds>` passed. As any class cannot have multiple methods with the same name, class cant have multiple metronomes with the same frequency. The number of milliseconds left for each of the metronomes can be accessed from `self._metronome_left[milliseconds]`


### `.start_timer(timer_name, duration, reset=True)`
Calling this would trigger `.on_timer_<timer_name>(self)` in `duration` milliseconds. If `.on_timer_<timer_name>(self)` is not declared,
exception would be raised. Example:

    def on_timer_explosion(self):
        # ...

    def put_explisve(self, x):
        # make an explosion in 10 seconds
        self.start_timer(explosion, 1000*10)

If `reset` is `True` (default), all previous triggers for this timer will be cancelled.

### `.delayed_setattr(attr, value, duration, reset=True)`
Calling this would trigger `setattr(self, attr, value)` in `duration` milliseconds. If `attr` is not declared,
exception would be raised. The purpose of this methods is to serve as an simpler alternative to start_timer. Example:

    def take_damage(self, x):
        if self.vulnerability:
            self.hp -= x
            # become invulnerable for 1 second
            self.vulnerability = False
            self.delayed_setattr('vulnerability', True, 1000)


This would be identical to

    def on_timer_vulnerability(self):
        self.vulnerability = True

    def take_damage(self, x):
        if self.vulnerability:
            self.hp -= x
            # become invulnerable for 1 second
            self.vulnerability = False
            self.start_timer('vulnerability', 1000)

If `reset` is `True` (default), all previous triggers for this attribute will be cancelled.

### `.delayed_setattr_seq(attr, pairs)`
Simplifies calling `.delayed_setattr` for one attributs. `pairs` should contain pairs of values and delays. Usefull for animation sequences.

    def start_animation(self):
        self.delayed_setattr_seq('frame', [(0, 0), (1, 100), (2, 200), (3, 300), (4, 400)])

    # this would be identical to

    def start_animation(self):
        self.delayed_setattr('frame', 0, 0)  # it there is any animation running, it will be cancelled
        self.delayed_setattr('frame', 1, 100, reset=False)
        self.delayed_setattr('frame', 2, 200, reset=False)
        self.delayed_setattr('frame', 3, 300, reset=False)
        self.delayed_setattr('frame', 4, 400, reset=False)



System Events
-------------

### `.on_window_resize(self, w, h)`
If implemented, this method would be called every time application window is resized. This can happen only if `PyGameWindow` is initialised with `resizable=True`. This methods receives two positional arguments: new width and height of the window.

### Implementing exit trigger
Custom function which is triggered on closing the application window can be implemented, but in a different way. Write the function, as pass in as an argument when creating `PyGameWindow`. Exit function is triggered before any other events.


Order of execution
------------------

Event handlers are executed in the following order:

1. pressed keys
2. held keys
3. update
4. metronomes
5. timers
6. delayed_setattr

Order of execution between objects is controlled by used calling `read_events()`

Redefining `read_events`
--------------------------

`read_events(events, time_passed, key_pressed)` can be redefined, for example, to call `read_events` of children objects. Call `.__read_events__(events, time_passed, status)` to access the underlying implementation. For example:

    class Character():
       # ...
       def read_events(self, events, time_passed, key_pressed):
           self.right_hand.read_events(events, time_passed, key_pressed)
           self.left_hand.read_events(events, time_passed, key_pressed)
           if not self.sleep:
               self.__read_events__(events, time_passed, key_pressed)


EventReaderAsClass
------------------

Instead of inheriting event handler class from `simplified_pygame.EventReader`, one can inherit from  `simplified_pygame.EventReaderAsClass`. In this case, no instance is needed, as handler would be called for the class methods instead object methods. This approach seems like a hack for me, I dont know if I should recommencement using it. But it seems nice for event handles which dont have any inner state.



Graphics
========

`simplified_pygame.Canvas`
----------------------------

This class implements several shortcuts for `pygame` graphics. Note that `simplified_pygame.PyGameWindow` is an instance of `simplified_pygame.Canvas`.

### `Canvas.fill(col)`
Fill the whole canvas with a given color.

### `Canvas.rect((x0, y0, width, height), col)`
### `Canvas.box((x0, y0, width, height), col)`
Draw a filled (`rect`) or not filled (`box`) rectangle of a given color.

### `Canvas.write(x, y, s, font=None, size=20, col=(100, 100, 100), pos='>', border=False, bold=False, italic=False)`
Write text `s` onto canvas. Text alignment is defined with the `pos` argument, and can be `'.'`, `'<'` or `'>'`.
Uses `Canvas.default_font`, unless other font is provided. Font with the given name is loaded from the `assets` sub directory. Text can contain multiple lines.

### `Canvas.sprite(self, x, y, sprite, scale=1)`
Paste a sprite onto canvas. `sprite` can be `pygame.Surface`, `simplified_pygame.Canvas` or sprite name.
Sprite name is searched in the `simplified_pygame.SPRITES` dictionary. If it is not found there, it is loaded from the `assets` sub directory.
### `Canvas.stack(self, sprite, x=0, y=0)`
Paste a sprite onto the copy of the canvas, and return the copy.

### `Canvas.copy(self)`
### `Canvas.crop(self, x, y, w, h)`
### `Canvas.rotate(self, angle)`
### `Canvas.flip(self, vertical_flip, horizontal_flip)`
Returns the canvas with the cropped, rotated or flipped surface.

### `Canvas.add_outline(self, col=(0, 0, 0))`
Adds a pixel-wide outline around the sprite

### `Canvas.replace_colors(self, mapping)`
Replaces the colors in the sprire according to the mapping

### `Canvas.with_offset(dx, dy)`
Return `simplified_pygame.Canvas` object referring to the same canvas, but every drawing method which would be called on it would be offset by (dx, dy) pixels.

### `Canvas.part(x, y, w, h, bg_col=(0, 0, 0, 0))`
### `Canvas.layer(bg_col=(0, 0, 0, 0))`
Separates a part of the surface. Can be used within a context manager:

    with Canvas.part(...) as subcanvas:
         # draw on subcanvas
    # when exiting from the context, subcanvas will be pasted back on the Canvas

This is useful when working with transparent colors. `part` returns a part of the surface, specified by coordinates; `layer` takes the whole surface.

### `simplified_pygame.Canvas.load(filename, corner_alpha=False)`
Constructor, loading the `.png` file with the given name. If `corner_alpha` is True, all pixels with the same color as top left corner would be treated as transparent.


`simplified_pygame.PyGameWindow`
----------------------------------
In additional to canvas properties, this class have the following methods


### `PyGameWindow.set_game_resolution(w, h)`
Set the resolution of the game, if it should be changed. Mouse events and drawing happens in relation to game resolution.

### `PyGameWindow.set_window_resolution(new_res)`
Set the resolution of the game window, as displayed. `new_res` can be a pair of `(width, height)`, a single number setting the scale between the  game resolution and window resolution, or `'fullscreen'`.


Sound
=====

### `simplified_pygame.play_sound(name, volume=1, repeat=0)`
Play a sound from a given file in the assets folder.

### `simplified_pygame.play_music(name, volume=1, channel=0)`
Start playing a music from a given file in the assets folder. Music plays on repeat.
There could be only one music file per channel. Calling the fuction again with the same argumets does nothing.
Calling the fuction again with the same file and channel but another volume will change we colume, but will not reset the music.
Calling the fuction again with the same channel but another file will stop the current file and start another one.

This function is designed to be called in the main loop, like this:

    for events, time_passed, key_pressed in WINDOW.main_loop(framerate=60):
        ...
        # this would be called 60 times per second, but the music will behave as expected
        if GAME_STATE == 'menu':
            simplified_pygame.play_music('menu_music')
        elif GAME_STATE == 'game':
            simplified_pygame.play_music('game_music')
        elif GAME_STATE == 'combat':
            simplified_pygame.play_music('combat_music')
        ....


### `simplified_pygame.stop_music(channel=0)`
Stop the music on a given channel:

### `simplified_pygame.mixer.volume`
This property can be used to controll master volume. Default is 0.5

Data Files
==========

Use `simplified_pygame.DataFile(filename, *args, **kwargs)` as dictionary which would be saved to the local file every time it is modified. `*args` and `**kwargs` define the values
of the dictionary for the first time the application is execute. Subsequently, values would be loaded from a file.
