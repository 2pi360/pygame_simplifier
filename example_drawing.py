import simplified_pygame


WINDOW = simplified_pygame.PyGameWindow(640, 480, bg_color=(100, 100, 100))


class AppControlls(simplified_pygame.EventReaderAsClass):

    def on_key_f1():
        WINDOW.set_window_resolution(1)
    def on_key_f2():
        WINDOW.set_window_resolution(2)
    def on_key_f3():
        WINDOW.set_window_resolution('fullscreen')
    def on_key_escape():
        WINDOW.on_exit()


def draw(canvas):
    # white lines
    canvas.box((10, 10, 200, 100), (255, 255, 255))
    # green text
    canvas.write(20, 20, 'This is\nan example', border=True, col=(0, 200, 0))

for events, time_passed, key_pressed in WINDOW.main_loop(framerate=10):
    AppControlls.read_events(events, time_passed, key_pressed)

    # normal draw
    draw(WINDOW)

    # draw with offset
    draw(WINDOW.with_offset(0, 120))

    # select a subspace of the canvas, and draw in it
    # everything drawn outsize the subspace will be discarded
    # this part is set to have red semi-transparent background
    with WINDOW.part(50, 140, 100, 100, bg_col=(200, 0, 0, 100)) as part:
        draw(part)




