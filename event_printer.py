import simplified_pygame


WINDOW = simplified_pygame.PyGameWindow(640, 480, bg_color=(0, 0, 0))

text = []

for events, time_passed, key_pressed in WINDOW.main_loop(framerate=1):
    if events or key_pressed:
        print(events, key_pressed)
        text = ['events'] + ['  '+str(x) for x in events] + ['key_pressed'] + ['  '+str(x) for x in key_pressed]
    WINDOW.write(10, 10, '\n'.join(text), font=None, col=(0, 200, 0))
