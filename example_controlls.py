import simplified_pygame


WINDOW = simplified_pygame.PyGameWindow(640, 480, bg_color=(100, 100, 100))



class AppControlls(simplified_pygame.EventReaderAsClass):

    def on_key_f1():
        WINDOW.set_window_resolution(1)
    def on_key_f2():
        WINDOW.set_window_resolution(2)
    def on_key_f3():
        WINDOW.set_window_resolution('fullscreen')
    def on_key_escape():
        WINDOW.exit()


class Box(simplified_pygame.EventReader):
    """ Box controlled by pressing keys on keyboard """
    def __init__(self):
        self.x = 0
        self.y = 0

    def on_key_right(self):
        self.x+=1
    def on_key_left(self):
        self.x-=1
    def on_key_up(self):
        self.y-=1
    def on_key_down(self):
        self.y+=1

    def draw(self):
        WINDOW.rect((self.x*10+100, self.y*10+100, 10, 10), col=(255, 0, 0))
        WINDOW.write(self.x*10+100, self.y*10+100, 'arrows', font=None, col=(0, 0, 0))


class Box2(simplified_pygame.EventReader):
    """ Box controlled by holding WASD on keyboard """
    def __init__(self):
        self.x = 100
        self.y = 100
        self.key_map = simplified_pygame.WASD_AS_ARROWS | simplified_pygame.DISABLE_ARROWS

    def on_hold_right(self, dur, time_passed):
        if dur > 200: self.x+=time_passed/2
    def on_hold_left(self, dur, time_passed):
        if dur > 200: self.x-=time_passed/2
    def on_hold_up(self, dur, time_passed):
        if dur > 200: self.y-=time_passed/2
    def on_hold_down(self, dur, time_passed):
        if dur > 200: self.y+=time_passed/2

    def draw(self):
        WINDOW.rect((self.x, self.y+100, 10, 10), col=(0, 255, 0))
        WINDOW.write(self.x, self.y+100, 'WASD hold', font=None, col=(0, 0, 0))


class Box3(simplified_pygame.EventReader):
    """ Box controlled by mouse """
    def __init__(self):
        self.x = 0
        self.y = 0

    def on_mouse_move(self, pos):
        x, y = pos
        self.x = x
        self.y = y
    def on_mouse_click(self, _):
        self.x += 10
    def on_mouse_rightclick(self, _):
        self.y += 10

    def draw(self):
        WINDOW.rect((self.x, self.y, 10, 10), col=(0, 0, 255))
        WINDOW.write(self.x, self.y, 'mouse', font=None, col=(0, 0, 0))


class Box4(simplified_pygame.EventReader):
    """ box conrolled by pressing keys on controller """
    def __init__(self):
        self.x = 1
        self.y = 4

    def on_key_joy_B(self):
        self.x+=1
    def on_key_joy_X(self):
        self.x-=1
    def on_key_joy_Y(self):
        self.y-=1
    def on_key_joy_A(self):
        self.y+=1

    def draw(self):
        WINDOW.rect((self.x*10+100, self.y*10+100, 10, 10), col=(255, 255, 0))
        WINDOW.write(self.x*10+100, self.y*10+100, 'controller', font=None, col=(0, 0, 0))


class Box5(Box):
    # box conrolled Dpad
    def __init__(self):
        self.x = 4
        self.y = 1
        self.key_map = simplified_pygame.DPAD_AS_ARROWS | simplified_pygame.DISABLE_ARROWS

    def draw(self):
        WINDOW.rect((self.x*10+100, self.y*10+100, 10, 10), col=(255, 0, 255))
        WINDOW.write(self.x*10+100, self.y*10+100, 'dpad', font=None, col=(0, 0, 0))


boxes = [Box(), Box2(), Box3(), Box4(), Box5()]


for events, time_passed, key_pressed in WINDOW.main_loop(framerate=60):
    AppControlls.read_events(events, time_passed, key_pressed)
    for box in boxes:
        box.read_events(events, time_passed, key_pressed)
    for box in boxes:
        box.draw()

