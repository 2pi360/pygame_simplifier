1.6
   * Added support for several controllers at once
   * Added support for holding mouse buttons
   * Default font color changed from gray to black
   * time_passed now can't exceed 1 second
   * Fixed some bugs and typos
   * instead of alt, ctrl and shift key events, there are now lalr, ralr etc

1.5
   * Added more draving functions for canvas: save(), lines(), circle(), write_vert()
   * Documented existing function line()
   * Added function in_box() to check whatever given point is in the given box

1.4
   * Sound and music part extended and refactored

1.3
   * added new file: run_to_release
   * bug with text border fixed again
   * bug with sprite blitting fixed
   * new drawing function: line
   * box and rect drawing function now accept negative values
   * new method for event reader: delayed_setattr_seq
   * new methods for canvas: copy, stack, add_outline, repalce_colors
   * refactoring

1.2
   * bug with text border fixed
   * weird bug with sprites fixed
   * more documentation
   * refactoring

1.1
   * new handler: .on_any_key()
   * new Canvas methods: flip(), crop(), rotate(), set_alpha()
   * new Canvas constructor: from_pygame_surface()
   * fixed examples
   * fixed text alignment
   * fixed Canvas.layer
   * fixed wrong inilialization of ._timers
   * refactoring

1.0
   * release
